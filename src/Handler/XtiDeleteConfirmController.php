<?php

/**
 * @file
 * Contains \Drupal\sxt_xtitems\Handler\XtiDeleteConfirmController
 */

namespace Drupal\sxt_xtitems\Handler;

/**
 */
class XtiDeleteConfirmController extends XtItemControllerBase {

  protected $node;
  
  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->node = \Drupal::request()->get('node');
    
    return 'Drupal\sxt_xtitems\Form\XtItemDeleteConfirmForm';
  }

  protected function getFormTitle() {
    return t('Delete item');
  }

  protected function getSubmitLabel() {
    return t('Delete');
  }

  protected function getDoneMessage() {
    return t('Item has been deleted.');
  }
  
}
