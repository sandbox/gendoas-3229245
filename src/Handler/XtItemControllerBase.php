<?php

/**
 * @file
 * Contains \Drupal\sxt_xtitems\Handler\XtItemControllerBase
 */

namespace Drupal\sxt_xtitems\Handler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\XtEditControllerBase;

/**
 */
abstract class XtItemControllerBase extends XtEditControllerBase {

  protected $node;
  
  abstract protected function getDoneMessage();
  
  /**
   */
  protected function hasLabels() {
    return TRUE;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    if ($form_state->isSubmitted() && !$form_state->hasAnyErrors()) {
      $slogxtData['wizardFinished'] = true;
      $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
    }

    return parent::buildContentResult($form, $form_state);
  }

  /**
   */
  protected function getOnWizardFinished() {
    $msg_all = $this->messenger->all();
    if (empty($msg_all['error'])) {
      $this->messenger->deleteAll();
      $this->messenger->addStatus($this->getDoneMessage());
    }

    return [
        'command' => 'sxt_slogitem::finishedNodeEdit',
        'args' => [
            'etype' => 'node',
            'eid' => $this->node->id(),
            'specialData' => [
                'command' => 'slogxt::rebuildPreviousPage',
            ],
        ],
    ];
  }

}
