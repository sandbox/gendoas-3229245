<?php

/**
 * @file
 * Contains \Drupal\sxt_xtitems\Handler\XtiSettingsController
 */

namespace Drupal\sxt_xtitems\Handler;

use Drupal\sxt_xtitems\Form\XtItemTrait;

/**
 */
class XtiSettingsController extends XtItemControllerBase {

  use XtItemTrait;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $request = \Drupal::request();
    $this->node = $request->get('node');
    return 'Drupal\sxt_xtitems\Form\XtItemSettingsForm';
  }

  protected function getFormTitle() {
    return t('Settings');
  }

  protected function getSubmitLabel() {
    return t('Save settings');
  }

  protected function getDoneMessage() {
    return t('Settings have been saved.');
  }
  
}
