<?php

/**
 * @file
 * Contains \Drupal\sxt_xtitems\Handler\XtiAddController
 */

namespace Drupal\sxt_xtitems\Handler;

use Drupal\sxt_xtitems\Form\XtItemTrait;

/**
 */
class XtiAddController extends XtItemControllerBase {

  use XtItemTrait;

  protected $region_name;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $request = \Drupal::request();
    $this->node = $request->get('node');
    $region_labels = $this->getRegionLabelsByNode($this->node);
    $region = $request->get('region');
    $this->region_name = $region_labels[$region] ?? '???';

    return 'Drupal\sxt_xtitems\Form\XtItemAddForm';
  }

  protected function getFormTitle() {
    return t('Add new for region %region', ['%region' => $this->region_name]);
  }

  protected function getSubmitLabel() {
    return t('Add new');
  }

  protected function getDoneMessage() {
    return t('New item(s) have been added.');
  }
  
}
