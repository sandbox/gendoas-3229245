<?php

/**
 * @file
 * Contains \Drupal\sxt_xtitems\SlogXtItems.
 */

namespace Drupal\sxt_xtitems;

use Drupal\node\NodeInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 */
class SlogXtItems implements TrustedCallbackInterface {

  const XTXSI_IDX_UNRATED = 0;
  const XTXSI_IDX_BAD = 1;
  const XTXSI_IDX_VALID = 2;
  const XTXSI_IDX_FINE = 3;
  const XTXSI_IDX_BEST = 4;
  //
  const XTXSI_NUM_DONE = 7;
  const XTXSI_NUM_DONE2 = 3;
  //
  const XTXSI_RLSET_DEFAULT = 'default';
  const XTXSI_RLSET_TODO = 'todo';

  /**
   */
  public static function trustedCallbacks() {
    return ['preRenderTextFormat'];
  }

  public static function getSeparator() {
    return '--';
  }

  public static function getRegionLabelsDefault() {
    $regions = [
        self::XTXSI_IDX_UNRATED => (string) t('Unrated'),
        self::XTXSI_IDX_BAD => (string) t('Bad'),
        self::XTXSI_IDX_VALID => (string) t('Valid'),
        self::XTXSI_IDX_FINE => (string) t('Fine'),
        self::XTXSI_IDX_BEST => (string) t('Best'),
    ];

    return array_reverse($regions, TRUE);
  }

  public static function getRegionLabelsToDo() {
    $regions = [
        self::XTXSI_IDX_UNRATED => (string) t('Unassigned'),
        self::XTXSI_IDX_BAD => (string) t('No'),
        self::XTXSI_IDX_VALID => (string) t('Done'),
        self::XTXSI_IDX_FINE => (string) t('Next'),
        self::XTXSI_IDX_BEST => (string) t('ToDo'),
    ];

    return array_reverse($regions, TRUE);
  }

  public static function getRegionLabelsAll($as_string = TRUE) {
    $label_sets = [
        self::XTXSI_RLSET_DEFAULT => self::getRegionLabelsDefault(),
        self::XTXSI_RLSET_TODO => self::getRegionLabelsToDo(),
    ];
    if ($as_string) {
      foreach ($label_sets as &$label_set) {
        $label_set = implode(';', $label_set);
      }    
    }
    
    return $label_sets;
  }

  public static function getRegionLabels($key, $as_string = TRUE) {
    $label_sets = self::getRegionLabelsAll($as_string);
    if (!empty($label_sets[$key])) {
      return $label_sets[$key];
    }
    
    return $label_sets[self::XTXSI_RLSET_DEFAULT];
  }

  public static function getXtItemsFormats() {
    $all_formats = filter_formats();
    $formats = array_filter($all_formats, function ($format) {
      $id = $format->id();
      if ($id === 'plain_text') {
        return TRUE;
      }
      return (substr($id, 0, 7) === 'xtitems');
    });

    ksort($formats);
    return $formats;
  }

  public static function xtitemsFormat() {
    $format_id = 'xtitems_text';
    $formats = self::getXtItemsFormats();
    $format_ids = array_keys($formats);
    if (!in_array($format_id, $format_ids)) {
      $format_id = (string) 'plain_text';
    }
    return $format_id;
  }

  public static function splitInput($input) {
    $sep = self::getSeparator();
    $input = trim($input);
    $array = [$input];
    if (substr($input, 0, 2) === $sep) {
      $input = trim($input, $sep);
      $array = explode($sep, $input);
      $array = array_filter(array_map('trim', $array));
    }

    return $array;
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   */
  public static function configEditFormAlter(array &$form, FormStateInterface $form_state) {
    $field = $form_state->getFormObject()->getEntity();
    if ($field->getName() === 'xtitext') {
      $formats = filter_formats();
      $format_id = 'xtitems_text';
      if (!empty($formats[$format_id])) {
        unset($form['default_value']);
        $format = $formats[$format_id];
        $options = [$format_id => $format->label()];
        $form['third_party_settings']['sxt_xtitems']['allowed_format'] = [
            '#type' => 'select',
            '#title' => t('Required format'),
            '#description' => t('Format %format is enforced.', ['%format' => $format->label()]),
            '#options' => $options,
            '#default_value' => $field->getThirdPartySetting('sxt_xtitems', 'allowed_format', $format_id),
        ];
      }
    }
  }

  public static function hookFieldXtiTextAlter(&$element, FormStateInterface $form_state) {
    $element['#base_type'] = 'textarea';
    $element['#rows'] = 3;

    $allowed_format = 'xtitems_text';
    $element['#format'] = $allowed_format;
    $element['#allowed_formats'] = [$allowed_format];

    $element['#pre_render'][] = [static::class, 'preRenderTextFormat'];
  }

  public static function preRenderTextFormat($element) {
    $element['format']['#attributes']['class'][] = 'visually-hidden';
    return $element;
  }

  public static function nodePresave(NodeInterface $node) {
    if ($node->hasField('xtitext')) {
      // enforce format
      $node->xtitext->format = self::xtitemsFormat();
    }
  }

}
