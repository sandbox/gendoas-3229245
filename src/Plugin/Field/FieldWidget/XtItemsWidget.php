<?php

/**
 * @file
 * Contains \Drupal\sxt_xtitems\Plugin\Field\FieldType\XtItemsWidget.
 */

namespace Drupal\sxt_xtitems\Plugin\Field\FieldWidget;

use Drupal\sxt_xtitems\SlogXtItems;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Url;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\sxt_xtitems\Form\XtItemTrait;

/**
 * @FieldWidget(
 *   id = "xtitems_widget",
 *   label = @Translation("XtItems"),
 *   description = @Translation(".............XtItems Widget"),
 *   field_types = {
 *     "xtitems_fieldtype"
 *   },
 *   multiple_values = TRUE
 * )
 */
class XtItemsWidget extends WidgetBase {

  use XtItemTrait;

  protected $savedInfo = [];

  /**
   */
  protected function getRouteName($for_xt_dialog, $action) {
    $prefix = $for_xt_dialog ? 'sxt_xtitems.ajax.xtitems' : 'sxt_xtitems.xtitems';
    return "$prefix.$action";
  }

  /**
   */
  protected function onMassageFormValues(array &$new_values, array $item_values) {
    //nothing by default
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_name = $this->getFieldName();
    $form_state->set($field_name, $items);

    $uid = (integer) \Drupal::currentUser()->id();
    $nid = $form_state->getFormObject()->getEntity()->id();
    $first = !empty($nid) ? $items->first() : FALSE;
    if (empty($nid) || empty($first)) {
      $element['#tree'] = TRUE;
      // add new node
      $args = ['%sep' => $this->getSeparator()];
      $element[0]['content'] = [
          '#type' => 'textarea',
          '#title' => t('Text'),
          '#description' => t('Create first items (max. 240 characters). Multiple items with leading "%sep"', $args),
          '#rows' => 4,
          '#maxlength' => 240,
          '#required' => TRUE,
      ];
      $element[0]['uid'] = [
          '#type' => 'hidden',
          '#value' => $uid,
      ];
      $element[0]['region'] = [
          '#type' => 'hidden',
          '#value' => 0,
      ];
      $element[0]['weight'] = [
          '#type' => 'hidden',
          '#value' => 0,
      ];

      // enforce published
      if (!empty($form['status']['widget']['value'])) {
        $status_value = &$form['status']['widget']['value'];
        $status_value['#default_value'] = 1;
        $status_value['#disabled'] = TRUE;
      }
      //add handler
      $form_state->set('xti_field_name', $this->getFieldName());
      $form['#validate'][] = [get_class(), 'formValidate'];
    } elseif (!$this->isDefaultValueWidget($form_state)) {
      // edit node
      $element += $this->buildBlocksForm($items, $form_state);
    }

    return $element;
  }

  /**
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $field_name = $form_state->get('xti_field_name');
    $uid = (integer) \Drupal::currentUser()->id();
    $node = $form_state->getFormObject()->getEntity();
    $xtitems = $node->get($field_name);

    // clear xtitems
    for ($i = $xtitems->count(); $i > 0; $i--) {
      $xtitems->removeItem($i - 1);
    }

    $values = $form_state->getValues();
    $input_value = (string) $values[$field_name][0]['content'];
    $input_array = SlogXtItems::splitInput($input_value);
    $items_number = count($input_array);
    $weight = round(-$items_number / 2);

    $new_values = [];
    foreach ($input_array as $input) {
      $new_value = [
          'content' => $input,
          'region' => SlogXtItems::XTXSI_IDX_UNRATED,
          'uid' => $uid,
          'weight' => $weight,
      ];
      $new_values[] = $new_value;
      $weight++;
    }
    $form_state->setValue([$field_name], $new_values);
  }

  /**
   * 
   * @param FieldItemListInterface $items
   * @param FormStateInterface $form_state
   * @return array
   */
  protected function buildBlocksForm(FieldItemListInterface $items, FormStateInterface $form_state) {
    $field_name = $this->getFieldName();
    $node = $form_state->getFormObject()->getEntity();
    $node_id = $node->id();
    $langcode = $form_state->get('langcode');

    // prepare route options
    $request = \Drupal::request();
    $for_xt_dialog = SlogXt::isAjaxRequest();
    $route_url = $request->getBaseUrl() . $request->getPathInfo();
    $this_destination = (string) $request->get('destination');
    if (!empty($this_destination)) {
      $this_destination = '?destination=' . $this_destination;
    }
    $route_options = [
        'query' => ['destination' => $route_url . $this_destination],
    ];

    $input = $form_state->getUserInput();
    $input_data = [];
    if (!empty($input[$field_name]) && !empty($input['blocks'])) {
      foreach ($input[$field_name] as $iid => $value) {
        $iid = (integer) $value['item_id'];
        $input_data[$iid] = $value;
        $input_data[$iid]['region'] = $input['blocks'][$iid]['region'];
      }
    }
    $form_state->set('input_data', $input_data);

    $form = [
        '#type' => 'table',
        '#header' => [
            $this->t('Content'),
            $this->t('Region'),
            $this->t('Weight'),
            $this->t('Operations'),
        ],
        '#attributes' => [
            'id' => 'blocks',
        ],
    ];

    //
    $items_number = $items->count();
    $can_delete = ($items_number > 1);
    $weight_delta = round($items_number / 2);
    $blocks = $this->prepareBlocksByRegion($items, $input_data);
    $regions = $this->getRegionLabelsByNode($node);
    $route_add = $this->getRouteName($for_xt_dialog, 'add');
    foreach ($regions as $region => $title) {
      $form['#tabledrag'][] = [
          'action' => 'match',
          'relationship' => 'sibling',
          'group' => 'block-region-select',
          'subgroup' => 'block-region-' . $region,
          'hidden' => FALSE,
      ];
      $form['#tabledrag'][] = [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'block-weight',
          'subgroup' => 'block-weight-' . $region,
      ];

      $form['region-' . $region] = [
          '#attributes' => [
              'class' => ['region-title', 'region-title-' . $region],
              'no_striping' => TRUE,
          ],
      ];

      $route_parameters = [
          'node' => $node_id,
          'region' => $region,
      ];

      $form['region-' . $region]['title'] = [
          '#theme_wrappers' => [
              'container' => [
                  '#attributes' => ['class' => 'region-title__action'],
              ],
          ],
          '#prefix' => $title,
          '#type' => 'link',
          '#title' => $this->t('Add new <span class="visually-hidden">in the %region region</span>', ['%region' => $title]),
          '#url' => Url::fromRoute($route_add, $route_parameters, $route_options),
          '#wrapper_attributes' => [
              'colspan' => 4,
          ],
          '#attributes' => [
              'class' => ['button', 'button--small xtsitems-add'],
          ],
      ];

      $form['region-' . $region . '-message'] = [
          '#attributes' => [
              'class' => [
                  'region-message',
                  'region-' . $region . '-message',
                  empty($blocks[$region]) ? 'region-empty' : 'region-populated',
              ],
          ],
      ];
      $form['region-' . $region . '-message']['message'] = [
          '#markup' => '<em>' . $this->t('No items in this region') . '</em>',
          '#wrapper_attributes' => [
              'colspan' => 4,
          ],
      ];

      if (!empty($blocks[$region])) {
        $region_items = $blocks[$region];
        foreach ($region_items as $info) {
          $delta = $info['delta'];
          $item_id = $info['item_id'];

          $form[$delta] = [
              '#attributes' => [
                  'class' => ['draggable'],
              ],
          ];
          $form[$delta]['#attributes']['class'][] = 'block-enabled';

          $form[$delta]['content'] = [
              '#type' => 'processed_text',
              '#text' => (string) $info['content'],
              '#format' => SlogXtItems::xtitemsFormat(),
              '#langcode' => $langcode,
              '#prefix' => '<div class="xtitem-content">',
              '#suffix' => '</div>',
              '#wrapper_attributes' => [
                  'class' => ['block', 'xtitem-block'],
              ],
          ];
          $form[$delta]['region-theme']['region'] = [
              '#type' => 'select',
              '#default_value' => $region,
              '#required' => TRUE,
              '#title' => $this->t('Region for @block block', ['@block' => $info['content']]),
              '#title_display' => 'invisible',
              '#options' => $regions,
              '#attributes' => [
                  'class' => ['block-region-select', 'block-region-' . $region],
              ],
              '#parents' => ['blocks', $item_id, 'region'],
          ];

          $form[$delta]['weight'] = [
              '#type' => 'weight',
              '#default_value' => $info['weight'],
              '#delta' => $weight_delta,
              '#title' => $this->t('Weight for @block block', ['@block' => $info['content']]),
              '#title_display' => 'invisible',
              '#attributes' => [
                  'class' => ['block-weight', 'block-weight-' . $region],
              ],
          ];

          $info += [
              'route_parameters' => [
                  'node' => $node_id,
                  'xtitem_id' => $item_id,
              ],
              'route_options' => $route_options,
              'for_xt_dialog' => $for_xt_dialog,
          ];
          $form[$delta]['operations'] = $this->buildOperations($info, $can_delete);

          $form[$delta]['item_id'] = [
              '#type' => 'hidden',
              '#value' => $item_id,
              '#wrapper_attributes' => [
                  'class' => ['visually-hidden'],
              ],
          ];
          $this->savedInfo[$item_id] = $info;
        }
      }
    }

    // Settings
    if (XtsiNodeTypeData::isXtNode($node)) {
      $route_settings = $this->getRouteName($for_xt_dialog, 'settings');
      $route_parameters = ['node' => $node_id];
      $prefix_element = [
          '#theme_wrappers' => [
              'container' => [
                  '#attributes' => ['class' => 'xtitems-settings-wrapper'],
              ],
          ],
          '#type' => 'link',
          '#title' => t('Edit settings'),
          '#url' => Url::fromRoute($route_settings, $route_parameters, $route_options),
          '#attributes' => [
              'class' => ['button', 'button--small xtsitems-settings'],
          ],
      ];
      $form['#prefix'] = \Drupal::service('renderer')->render($prefix_element);
    }

    // using block functionality
    $form['#attached']['library'][] = 'block/drupal.block';
    $form['#attached']['library'][] = 'block/drupal.block.admin';
    return $form;
  }

  /**
   * 
   * @param array $info
   * @return array
   */
  public function buildOperations(array $info, $can_delete) {
    $build = [
        '#type' => 'operations',
        '#links' => $this->getOperations($info, $can_delete),
        '#attributes' => [
            'class' => ['xtsitems-ops'],
        ],
    ];

    return $build;
  }

  /**
   * 
   * @param array $info
   * @return array
   */
  public function getOperations(array $info, $can_delete) {
    $operations = [];
    $route_parameters = $info['route_parameters'];
    $route_options = $info['route_options'];
    $for_xt_dialog = $info['for_xt_dialog'];

    // todo: set $can_edit
    $can_edit = TRUE;
    if ($can_edit) {
      $route_name = $this->getRouteName($for_xt_dialog, 'edit');
      $operations['edit'] = [
          'title' => $this->t('Edit'),
          'weight' => 0,
          'url' => Url::fromRoute($route_name, $route_parameters, $route_options),
      ];
    }

    if ($can_delete) {
      // todo: set $can_delete
    }
    if ($can_delete) {
      $route_name = $this->getRouteName($for_xt_dialog, 'delete');
      $operations['delete'] = [
          'title' => $this->t('Delete'),
          'weight' => 1,
          'url' => Url::fromRoute($route_name, $route_parameters, $route_options),
      ];
    }

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $field_name = $this->getFieldName();
    $input_data = $form_state->get('input_data');
    if (!empty($input_data)) {
      $new_values = [];
      $idx = 0;
      $items = $form_state->get($field_name);
      foreach ($items as $delta => $item) {
        $item_values = $item->getValue();
        $item_id = (integer) ($item_values['item_id'] ?? 0);
        $region = (integer) $item_values['region'];
        $weight = (integer) $item_values['weight'];
        if (!empty($input_data[$item_id])) {
          $region = (integer) $input_data[$item_id]['region'] ?? $region;
          $weight = (integer) $input_data[$item_id]['weight'] ?? $weight;
        }

        $new_values[$idx] = [
            'region' => $region,
            'content' => (string) $item_values['content'],
            'item_id' => $item_id,
            'uid' => (integer) $item_values['uid'],
            'weight' => $weight,
        ];
        $this->onMassageFormValues($new_values[$idx], $item_values);
        $idx++;
      }

      return $new_values;
    } else {
      return parent::massageFormValues($values, $form, $form_state);
    }
  }

}
