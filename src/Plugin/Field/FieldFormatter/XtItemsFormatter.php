<?php

/**
 * @file
 * Contains \Drupal\sxt_xtitems\Plugin\Field\FieldFormatter\XtItemsFormatter.
 */

namespace Drupal\sxt_xtitems\Plugin\Field\FieldFormatter;

use Drupal\sxt_xtitems\SlogXtItems;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\sxt_xtitems\Form\XtItemTrait;

/**
 * Plugin implementation of the XtItems formatter.
 *
 * @FieldFormatter(
 *   id = "xtitems_formatter",
 *   label = @Translation("XtItems"),
 *   description = @Translation("XtItems Formatter"),
 *   field_types = {
 *     "xtitems_fieldtype"
 *   }
 * )
 */
class XtItemsFormatter extends FormatterBase {

  use XtItemTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $blocks = $this->prepareBlocksByRegion($items);
    $regions_all = $this->getRegionLabelsFromRequest();
    if (!$regions_all || count($regions_all) !== 5) {
      $regions_all = $this->getRegionLabelsDefault();
    }

    $regions = array_filter($regions_all, function ($idx) use ($blocks) {
      return !empty($blocks[$idx]);
    }, ARRAY_FILTER_USE_KEY);

    $add_toggler = (count($regions) > 1);
    $open_done = FALSE;
    foreach ($regions as $region => $title) {
      $count = count($blocks[$region]);
      $elements[$region] = [
          '#type' => 'details',
          '#title' => "$title ($count)",
          '#open' => (!$open_done || $region > SlogXtItems::XTXSI_IDX_VALID),
          '#attributes' => [
              'id' => "xtitems-block-$region",
              'class' => ['xtitems-block', "xtitems-block-$region"],
          ],
      ];
      if ($add_toggler) {
        $elements[$region]['#prefix'] = '<div class="xtitems-open-toggler"></div>';
      }
      $open_done = TRUE;

      foreach ($blocks[$region] as $info) {
        $delta = $info['delta'];
        $elements[$region][$delta] = $this->contentBuild($info, $langcode);
      }
    }

    $elements['#attached']['library'][] = 'sxt_xtitems/sxt_xtitems.core';
    return $elements;
  }

}
