<?php

/**
 * @file
 * Contains \Drupal\sxt_xtitems\Plugin\Field\FieldType\XtItemsFieldType.
 */

namespace Drupal\sxt_xtitems\Plugin\Field\FieldType;

use Drupal\sxt_xtitems\SlogXtItems;
use Drupal\sloggen\SlogGen;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;

/**
 * @FieldType(
 *   id = "xtitems_fieldtype",
 *   label = @Translation("XtItems"),
 *   description = @Translation(".................XtItems Field Type."),
 *   default_formatter = "xtitems_formatter",
 *   default_widget = "xtitems_widget"
 * )
 */
class XtItemsFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'content';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['item_id'] = DataDefinition::create('integer')
            ->setLabel(t('Instance ID'))
            ->setSetting('unsigned', TRUE)
            ->setInternal(TRUE)
            ->setRequired(FALSE);

    $properties['content'] = DataDefinition::create('string')
            ->setLabel(t('Content'))
            ->setRequired(FALSE);

    $properties['uid'] = DataDefinition::create('integer')
            ->setLabel(t('Owner'))
            ->setSetting('unsigned', TRUE);

    $properties['region'] = DataDefinition::create('integer')
            ->setLabel(t('Region'))
            ->setSetting('unsigned', TRUE);

    $properties['weight'] = DataDefinition::create('integer')
            ->setLabel(t('Weight'))
            ->setSetting('unsigned', FALSE);

    $properties['xtra'] = DataDefinition::create('string')
            ->setLabel(t('More instance related serialized data'))
            ->setInternal(TRUE)
            ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
        'columns' => [
            'item_id' => [
                'type' => 'int',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ],
            'content' => [
                'type' => 'varchar',
                'length' => 255,
            ],
            'uid' => [
                'type' => 'int',
                'unsigned' => TRUE,
            ],
            'region' => [
                'type' => 'int',
                'size' => 'tiny',
                'unsigned' => TRUE,
                'default' => 0,
            ],
            'weight' => [
                'type' => 'int',
                'unsigned' => FALSE,
                'default' => 0,
            ],
            'xtra' => [
                'type' => 'blob',
                'size' => 'normal',
                'serialize' => TRUE,
                'not null' => FALSE,
            ],
        ],
        'indexes' => [
            'content' => ['content'],
        ],
        'unique keys' => [
            'item_id' => ['item_id'],
        ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = SlogGen::getRandom();
    $user_ids = SlogGen::getRandomUserIds(1);

    $value = [
        'content' => $random->sentences(mt_rand(4, 20)),
        'region' => mt_rand(SlogXtItems::XTXSI_IDX_UNRATED, SlogXtItems::XTXSI_IDX_BEST),
        'uid' => $user_ids[0],
        'weight' => mt_rand(0, 500),
    ];

    return $value;
  }

}
