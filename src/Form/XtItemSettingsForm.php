<?php

/**
 * @file
 * Contains \Drupal\sxt_xtitems\Form\XtItemSettingsForm.
 */

namespace Drupal\sxt_xtitems\Form;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_xtitems\SlogXtItems;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\XtExtrasTrait;

/**
 * This is a dummy form, for development and extra actions only.
 */
class XtItemSettingsForm extends FormBase {

  use XtItemTrait;
  use XtExtrasTrait;

  protected $node;
  protected $new_region_labels;

  /**
   */
  public function getFormId() {
    return 'sxt_xtitems_xtitem_settings';
  }

  /**
   */
  public function isTaract() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $this->node = $request->get('node');

    $options = SlogXtsi::optsXtItemsRegionLabels($this->isTaract());
    array_unshift($options, '');
    $form['xtitems_preset'] = [
        '#type' => 'select',
        '#id' => 'xtitems_preset',
        '#title' => t('Preset'),
        '#description' => t('Select a preset to fill the fields.'),
        '#options' => $options,
        '#default_value' => 0,
    ];

    $regions = $this->getRegionLabelsByNode($this->node);
    foreach ($regions as $region => $title) {
      $field_id = "xtitems_region_$region";
      $idx = SlogXtItems::XTXSI_IDX_BEST - $region + 1;
      $form[$field_id] = [
          '#type' => 'textfield',
          '#id' => $field_id,
          '#title' => t('Region %idx:', ['%idx' => $idx]),
          '#wrapper_attributes' => ['class' => ['xt-label-inline']],
          '#default_value' => $title,
          '#size' => 20,
          '#required' => TRUE,
      ];
    }

    $this->addFieldActionSubmit($form, t('save'));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $region_labels = [];
    for ($idx = SlogXtItems::XTXSI_IDX_BEST; $idx >= SlogXtItems::XTXSI_IDX_UNRATED; $idx--) {
      $field_id = "xtitems_region_$idx";
      $region_labels[] = $form_state->getValue($field_id);
    }

    $new_labels = array_filter(array_map('trim', $region_labels));
    if (count($new_labels) !== 5) {
      $form_state->setErrorByName('none', t('Each region must not be empty.'));
    } else {
      $this->new_region_labels = implode(';', $new_labels);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    XtsiNodeTypeData::nodeSetXtra($this->node, $this->new_region_labels, 'region_label');
    $this->node->save();
  }

}
