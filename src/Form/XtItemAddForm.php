<?php

/**
 * @file
 * Contains \Drupal\sxt_xtitems\Form\XtItemAddForm.
 */

namespace Drupal\sxt_xtitems\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\XtExtrasTrait;

/**
 * This is a dummy form, for development and extra actions only.
 */
class XtItemAddForm extends FormBase {

  use XtItemTrait;
  use XtExtrasTrait;

  protected $node;
  protected $for_region = 0;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_xtitems_xtitem_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $this->node = $request->get('node');
    $this->for_region = $region = (integer) $request->get('region');

    $this->addFieldXtContent($form, 'content', t('Text'), NULL, $region);
    $form['content']['#required'] = TRUE;
    $this->addFieldPrepend($form);
    $this->addFieldActionSubmit($form, t('Add new'));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uid = (integer) \Drupal::currentUser()->id();
    $values = $form_state->getValues();
    $prepend = (boolean) $values['prepend'];
    $field_name = $this->getFieldName();
    $xtitems = $this->node->get($field_name);

    $input_value = (string) $values['content'];
    $input_array = $this->splitInput($input_value);
    $input_count = count($input_array);

    $weight = 0;
    $minmax = $this->getMinMaxForRegion($xtitems, $this->for_region);
    extract($minmax);
    if (!$is_empty) {
      $weight = $prepend ? $min - $input_count : $max + 1;
    }

    foreach ($input_array as $input) {
      $new_value = [
          'content' => $input,
          'region' => $this->for_region,
          'uid' => $uid,
          'weight' => $weight,
      ];
      $xtitems->appendItem($new_value);
      $weight++;
    }
    $this->node->save();

    $msg = $input_count > 1 ? t('Items have been added.') : t('Item has been added.');
    \Drupal::messenger()->addStatus($msg);
  }

}
