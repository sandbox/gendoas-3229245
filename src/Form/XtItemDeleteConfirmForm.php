<?php

/**
 * @file
 * Contains \Drupal\sxt_xtitems\Form\XtItemDeleteConfirmForm.
 */

namespace Drupal\sxt_xtitems\Form;

use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\ConfirmFormBase;

/**
 */
class XtItemDeleteConfirmForm extends ConfirmFormBase {

  use XtItemTrait;

  protected $node;
  protected $delta = 0;
  protected $xtitems;
  protected $values;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_xtitems_xtitem_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Delete item in node: %node', ['%node' => $this->node->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.node.canonical', ['node' => $this->node->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $region_labels = $this->getRegionLabelsByNode($this->node);
    $region = (integer) $this->values['region'];
    $content = $this->getTextFriendly((string) $this->values['content']);
    $args = ['%region' => $region_labels[$region]];
    $msg =  $this->t('You are about to delete item in region "%region":', $args)  //
            . "<br />- <strong><em>$content</em></strong>" . "<hr />" . parent::getDescription();
    return SlogXt::htmlMessage($msg, 'warning');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $this->node = $request->get('node');
    $field_name = $this->getFieldName();
    $xtitems = $this->node->get($field_name);
    $xtitem_id = (integer) $request->get('xtitem_id');
    $this->delta = $this->getDeltaFromItemId($xtitems, $xtitem_id);
    $xtitem = $xtitems->get($this->delta);
    if ($xtitem) {
      $this->values = $xtitem->getValue();
      return parent::buildForm($form, $form_state);
    }
    else {
      $args = [
          '%nid' => $this->node->id(),
          '%itemid' => $xtitem_id,
      ];
      $msg = t('Item not found (nid=%nid, iid=%itemid)', $args);
      $form['message'] = [
          '#type' => 'markup',
          '#markup' => SlogXt::htmlMessage($msg, 'error'),
      ];

      return $form;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $field_name = $this->getFieldName();
    $xtitems = $this->node->get($field_name);
    $xtitems->removeItem($this->delta);
    $this->node->save();
  }

}
