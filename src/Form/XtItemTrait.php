<?php

/**
 * @file
 * Contains \Drupal\sxt_xtitems\Form\XtItemTrait.
 */

namespace Drupal\sxt_xtitems\Form;

use Drupal\sxt_xtitems\SlogXtItems;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\slogxt\SlogXt;
use Drupal\node\NodeInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;

/**
 */
trait XtItemTrait {

  protected function getBundle() {
    return 'xtitems';
  }

  protected function getFieldName() {
    return 'xtitems';
  }

  protected function getSeparator() {
    return SlogXtItems::getSeparator();
  }

  public function getXtItemsFormats() {
    return SlogXtItems::getXtItemsFormats();
  }

  public function xtitemsFormat() {
    return SlogXtItems::xtitemsFormat();
  }

  protected function splitInput($input) {
    return SlogXtItems::splitInput($input);
  }

  protected function getRegionLabelsDefault() {
    return SlogXtItems::getRegionLabelsDefault();
  }

  protected function getRegionLabelsByNode(NodeInterface $node) {
    return XtsiNodeTypeData::getXtiRegionLabels($node);
  }

  protected function getRegionLabelsFromRequest() {
    return \Drupal::request()->get('xtitems_regions');
  }

  protected function getTextFriendly(string $text) {
    $search = "/\['\[[^\]]*\]'\]/";
    return preg_replace($search, '(.^.)', $text);
  }

  protected function prepareXtsiMoreLink($content, $delta, $itemid) {
    $search_part = "['[XtsiMoreLink:sid|id=0";
    $replace = $search_part . "|delta=$delta|xtitemid=$itemid";
    return slogxt_str_replace($search_part, $replace, $content);
  }

  protected function contentBuild(array $info, $langcode) {
    $delta = $info['delta'];
    $itemid = $info['item_id'];
    $content = (string) $info['content'];
    $element = [
        '#type' => 'processed_text',
        '#text' => $this->prepareXtsiMoreLink($content, $delta, $itemid),
        '#format' => $this->xtitemsFormat(),
        '#langcode' => $langcode,
        '#prefix' => '<div class="xtitems-item">',
        '#suffix' => '</div>',
    ];

    return $element;
  }

  protected function addFieldXtContent(array &$form, $key, $title, $value = NULL, $region = NULL) {
    $description = t('Item content (max. 240 characters).');
    if ($key === 'content' && isset($region) && !isset($value)) {
      $regions = $this->getRegionLabelsByNode($this->node);
      $args = ['%region' => $regions[$region], '%sep' => $this->getSeparator()];
      $description = t('Add new item in region %region (max. 240 characters). Multiple items with leading "%sep"', $args);
    }

    $for_edit = [];
    if (isset($value)) {
      $for_edit = [
          '#default_value' => $value,
          '#rows' => 4,
      ];
    }

    $form[$key] = [
        '#type' => 'textarea',
        '#title' => $title,
        '#description' => $description,
        '#maxlength' => 240,
            ] + $for_edit + $this->getInputFieldWrapper();
  }

  protected function addFieldActionSubmit(array &$form, $label) {
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $label,
        '#submit' => ['::submitForm'],
    ];
  }

  protected function prepareInfoContents(array $values) {
    $contents = [
        'content' => $values['content'],
    ];
    return $contents;
  }

  protected function getRatingOptions($off = 1) {
    return [
        SlogXtItems::XTXSI_IDX_UNRATED + $off => '__' . (string) t('unrated') . '__',
        SlogXtItems::XTXSI_IDX_BAD + $off => (string) t('Bad (unsuitable for this listing)'),
        SlogXtItems::XTXSI_IDX_VALID + $off => (string) t('Valid'),
        SlogXtItems::XTXSI_IDX_FINE + $off => (string) t('Fine'),
        SlogXtItems::XTXSI_IDX_BEST + $off => (string) t('Excellent'),
    ];
  }

  protected function getItemIds(FieldItemListInterface $xtitems, $all = FALSE) {
    $xtitem_ids = [];
    foreach ($xtitems as $delta => $xtitem) {
      $values = $xtitem->getValue();
      $item_id = $values['item_id'];
      $region = (integer) $values['region'];
      if (!empty($item_id) && ($all || $region !== SlogXtItems::XTXSI_IDX_BAD)) {
        $xtitem_ids[] = $item_id;
      }
    }

    return $xtitem_ids;
  }

  protected function getDeltaFromItemId(FieldItemListInterface $xtitems, $xtitemid) {
    $xtitemid = (integer) $xtitemid;
    foreach ($xtitems as $delta => $item) {
      $values = $item->getValue();
      $test_id = (integer) ($values['item_id'] ?? 0);
      if ($test_id === $xtitemid) {
        return $delta;
      }
    }

    return -1;
  }

  protected function getStateRatedInfo(FieldItemListInterface $xtitems, $region) {
    $uid = $this->user_id;
    $user_has_unrated = FALSE;
    $rating_num = $rating_sum = $items_done1 = $items_done2 = 0;
    $items = $this->getItemsByRegion($xtitems, $region);
    $items_all = count($items);
    foreach ($items as $item) {
      $xtra = $item['xtra'] ?? [];
      $num_done = $this->getNumDoneRatings($xtra);
      if (!$user_has_unrated && !$this->hasUserRated($uid, $xtra)) {
        $user_has_unrated = TRUE;
      }

      if ($num_done > 0) {
        if ($num_done >= SlogXtItems::XTXSI_NUM_DONE) {
          $items_done1++;
        } elseif ($num_done >= SlogXtItems::XTXSI_NUM_DONE2) {
          $items_done2++;
        }
        $rating_num += $num_done;
        $rating_sum += $xtra['state']['rated']['rating'];
      }
    }

    $unrated = $user_has_unrated ? t('Yes') : t('No');
    if ($items_done1 === $items_all) {
      $rated = "$items_done1/$items_all";
    } else {
      $rated = "$items_done1-$items_done2/$items_all";
    }
    $avarage = ($rating_num > 0) ? $rating_sum / $rating_num : 0;
    $result = [
        'unrated' => t('Unrated') . ': ' . SlogXt::htmlHighlightText($unrated, $user_has_unrated),
        'rated' => t('Rated') . ': ' . SlogXt::htmlHighlightText($rated),
        'single' => t('Single ratings') . ': ' . SlogXt::htmlHighlightText($rating_num),
    ];
    if ($avarage > 0) {
      $avarage = number_format($avarage * 25, 1);
      $result['avarage'] = t('Avarage') . ': ' . SlogXt::htmlHighlightText("$avarage%");
    }

    return implode('; ', $result);
  }

  protected function prepareContentItemsXti(array $content_items, array $slogitem_nodes) {
    $test_bundle = $this->getBundle();
    $for_xticollect = (boolean) \Drupal::request()->get('for_xticollect');
    foreach ($content_items as &$c_item) {
      $sid = (integer) ($c_item['entityid'] ?? 0);
      $node = $slogitem_nodes[$sid] ?? FALSE;
      $field_name = $this->getFieldName();
      if ($node && $for_xticollect) {
        $field_name = $test_bundle = $node->bundle();
      }
      if ($node && $node->bundle() === $test_bundle) {
        $xtitems = $node->get($field_name);
        $info = $this->getStateRatedInfo($xtitems, SlogXtItems::XTXSI_IDX_UNRATED);
        if (!empty($info)) {
          $c_item['liDescription'] = $info;
        }
        $xti_text = $node->get('xtitext')->value;
        if (!empty($xti_text)) {
          $c_item['liDetails'] = $this->getTextFriendly($xti_text);
        }
      }
    }

    return $content_items;
  }

  protected function getXtiValuesByRegion(FieldItemListInterface $xtitems) {
    $xti_values = [];
    foreach ($xtitems as $delta => $item) {
      $values = $item->getValue();
      $item_id = (integer) ($values['item_id'] ?? 0);
      $region = (integer) $values['region'];
      if ($item_id > 0) {
        $xti_values[$region][$item_id] = $values;
      }
    }

    return $xti_values;
  }

  protected function prepareBlocksByRegion(FieldItemListInterface $xtitems, array $input_data = []) {
    $blocks = [];
    foreach ($xtitems as $delta => $item) {
      $values = $item->getValue();
      $item_id = (integer) ($values['item_id'] ?? 0);
      $region = (integer) $values['region'];
      $weight = (integer) $values['weight'];
      if (!empty($input_data[$item_id])) {
        $region = (integer) $input_data[$item_id]['region'] ?? $region;
        $weight = (integer) $input_data[$item_id]['weight'] ?? $weight;
      }

      $contents = $this->prepareInfoContents($values);
      $blocks[$region][$delta] = $contents + [
          'delta' => $delta,
          'region' => $region,
          'item_id' => $item_id,
          'uid' => (integer) $values['uid'],
          'weight' => $weight,
          'xtra' => (array) ($values['xtra'] ?? []),
      ];
    }

    foreach ($blocks as $idx => $block) {
      if (!empty($block) && count($block) > 1) {
        uasort($block, ['\Drupal\Component\Utility\SortArray', 'sortByWeightElement']);
        $blocks[$idx] = $block;
      }
    }

    return $blocks;
  }

  protected function getContentByItemId(FieldItemListInterface $xtitems) {
    $result = [];
    foreach ($xtitems as $delta => $item) {
      $values = $item->getValue();
      $item_id = (integer) $values['item_id'];
      $result[$item_id] = $values['content'];
    }
    return $result;
  }

  protected function getItemsByRegion(FieldItemListInterface $xtitems, $region) {
    $result = [];
    foreach ($xtitems as $delta => $item) {
      $values = $item->getValue();
      if ((integer) $values['region'] === $region) {
        $item_id = (integer) $values['item_id'];
        $values['delta'] = $delta;
        $result[$item_id] = $values;
      }
    }

    if (!empty($result) && count($result) > 1) {
      uasort($result, ['\Drupal\Component\Utility\SortArray', 'sortByWeightElement']);
    }
    return $result;
  }

  protected function getSortedByRatings(array $values) {
    $result = [];
    foreach ($values as $item_id => $item_values) {
      $done = $this->getNumDoneRatings($item_values['xtra']);
      $item_values['weight'] += $done * 1000;
      $result[$item_id] = $item_values;
    }

    if (!empty($result) && count($result) > 1) {
      uasort($result, ['\Drupal\Component\Utility\SortArray', 'sortByWeightElement']);
    }
    return $result;
  }

  protected function setXtItemUserRating($uid, $rating, $xtra = []) {
    if (!$this->hasUserRated($uid, $xtra) && $rating > SlogXtItems::XTXSI_IDX_UNRATED) {
      if (!$xtra || !is_array($xtra['state']['rated']['user'])) {
        $xtra['state'] = $xtra['state'] ?? [];
        $xtra['state']['rated'] = $xtra['state']['rated'] ?? [];
        $xtra['state']['rated']['user'] = [];
        $xtra['state']['rated']['rating'] = 0;
      }
      $xtra['state']['rated']['user'][] = (integer) $uid;
      if ($rating > 1) {  // no point for 'BAD'
        $xtra['state']['rated']['rating'] += (integer) $rating;
      }

      return $xtra;
    }

    return FALSE;
  }

  protected function unsetRatedState($xtra = []) {
    if (isset($xtra['state']['rated'])) {
      unset($xtra['state']['rated']);
      if (empty($xtra['state'])) {
        unset($xtra['state']);
      }
    }

    return (!empty($xtra) ? $xtra : NULL);
  }

  protected function hasUserRated($uid, $xtra = []) {
    if (isset($xtra['state']['rated']['user']) && is_array($xtra['state']['rated']['user'])) {
      return in_array($uid, $xtra['state']['rated']['user']);
    }

    return FALSE;
  }

  protected function getUserHasUnrated($uid, FieldItemListInterface $xtitems) {
    $items = $this->getItemsByRegion($xtitems, SlogXtItems::XTXSI_IDX_UNRATED);
    foreach ($items as $item) {
      $xtra = $item['xtra'] ?? [];
      if (!$this->hasUserRated($uid, $xtra)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  protected function getNumDoneRatings($xtra = []) {
    if (isset($xtra['state']['rated']['user'])) {
      $rated_user = $xtra['state']['rated']['user'] ?? 0;
      $r_num = is_array($rated_user) ? count($rated_user) : (integer) $rated_user;
      return $r_num;
    }

    return 0;
  }

  protected function getUnratedItemsFromUser(FieldItemListInterface $xtitems, $uid) {
    $unrated_items = [];
    $uid = (integer) $uid;
    $blocks = $this->prepareBlocksByRegion($xtitems);
    $unrated = $blocks[SlogXtItems::XTXSI_IDX_UNRATED] ?? [];

    foreach ($unrated as $xtitem) {
      if ($xtitem['uid'] === $uid) {
        $item_id = $xtitem['item_id'];
        $unrated_items[$item_id] = $xtitem;
      }
    }

    return $unrated_items;
  }

  protected function getMinMaxForRegion(FieldItemListInterface $xtitems, $for_region) {
    $is_empty = TRUE;
    $min = $max = 0;
    foreach ($xtitems as $item) {
      $item_values = $item->getValue();
      if ((integer) $item_values['region'] === $for_region) {
        $is_empty = FALSE;
        $item_weight = (integer) $item_values['weight'];
        if ($min > $item_weight) {
          $min = $item_weight;
        }
        if ($max < $item_weight) {
          $max = $item_weight;
        }
      }
    }

    return [
        'min' => $min,
        'max' => $max,
        'is_empty' => $is_empty,
    ];
  }

  protected function finalizePreRatings(FieldItemListInterface $xtitems, &$changed) {
    $ratings = [];
    $changed = FALSE;
    $target_region = (string) SlogXtItems::XTXSI_IDX_UNRATED;
    foreach ($xtitems as $item) {
      $item_values = $item->getValue();
      $item_id = (integer) $item_values['item_id'];
      $xtra = $item_values['xtra'] ?? [];
      $user_is_array = !empty($xtra['state']['rated']['user']) && is_array($xtra['state']['rated']['user']);
      $num_user = $user_is_array //
              ? count($xtra['state']['rated']['user']) //
              : (integer) ($xtra['state']['rated']['user'] ?? 0);
      if ($item_id > 0 && $item_values['region'] === $target_region //
              && $num_user >= SlogXtItems::XTXSI_NUM_DONE2
      ) {
        $rating = (integer) $xtra['state']['rated']['rating'];
        $percent = (float) number_format($rating * 25 / $num_user, 1);

        if ($user_is_array) {
          $changed = TRUE;
          $xtra['state']['rated']['user'] = $num_user;
          $item_values['xtra'] = $xtra;
          $item->setValue($item_values, FALSE);
        }

        if ($percent < 40) {
          $changed = TRUE;
          $item_values['region'] = SlogXtItems::XTXSI_IDX_BAD;
          $xtra = $item_values['xtra'] ?? [];
          if (!empty($xtra['state']['rated'])) {
            unset($xtra['state']['rated']);
            if (empty($xtra['state'])) {
              unset($xtra['state']);
            }
          }
          $item_values['xtra'] = !empty($xtra) ? $xtra : NULL;
          $item->setValue($item_values, FALSE);
        } else {
          $ratings[$item_id] = [
              'weight' => (integer) ($percent * 10),
              'percent' => $percent,
              'done' => $num_user >= SlogXtItems::XTXSI_NUM_DONE,
          ];
        }
      }
    }

    if (!empty($ratings) && count($ratings) > 1) {
      uasort($ratings, ['\Drupal\Component\Utility\SortArray', 'sortByWeightElement']);
      $ratings = array_reverse($ratings, TRUE);
      foreach ($ratings as &$rating) {
        unset($rating['weight']);
      }
    }

    return $ratings;
  }

  /**
   * 
   * @param FieldItemInterface $xtitem
   * @return type
   */
  protected function getXtsiMapkey(FieldItemInterface $xtitem) {
    $item_values = $xtitem->getValue();
    $xtra = $item_values['xtra'] ?? [];
    return ($xtra['xtsi_mapkey'] ?? FALSE);
  }

  protected function setXtsiMapkey(FieldItemInterface $xtitem, $mapkey) {
    $item_values = $xtitem->getValue();
    $xtra = $item_values['xtra'] ?? [];
    $xtra['xtsi_mapkey'] = $mapkey;
    $item_values['xtra'] = $xtra;
    $xtitem->setValue($item_values, FALSE);
    return $xtitem;
  }

  protected function getXtsiMapkeys(FieldItemListInterface $xtitems) {
    $mapkeys = [];
    foreach ($xtitems as $item) {
      $item_values = $item->getValue();
      $xtra = $item_values['xtra'] ?? [];
      if (!empty($xtra['xtsi_mapkey'])) {
        $mapkeys[] = $xtra['xtsi_mapkey'];
      }
    }
    return $mapkeys;
  }

  protected function getItemByXtsiMapkey(FieldItemListInterface $xtitems, $mapkey) {
    foreach ($xtitems as $item) {
      $item_values = $item->getValue();
      $xtra = $item_values['xtra'] ?? [];
      if (($xtra['xtsi_mapkey'] ?? FALSE) === $mapkey) {
        return $item;
      }
    }
    return FALSE;
  }

}
