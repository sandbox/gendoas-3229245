<?php

/**
 * @file
 * Contains \Drupal\sxt_xtitems\Form\XtItemEditForm.
 */

namespace Drupal\sxt_xtitems\Form;

use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\XtExtrasTrait;

/**
 */
class XtItemEditForm extends FormBase {

  use XtItemTrait;
  use XtExtrasTrait;

  protected $node;
  protected $delta = 0;
  protected $input;
  protected $old_content;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_xtitems_xtitem_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $this->node = $request->get('node');
    $xtitem_id = (integer) $request->get('xtitem_id');
    $field_name = $this->getFieldName();
    $xtitems = $this->node->get($field_name);
    $this->delta = $this->getDeltaFromItemId($xtitems, $xtitem_id);
    $xtitem = $xtitems->get($this->delta);
    if ($xtitem) {
      $values = $xtitem->getValue();

      $r_num = $this->getNumDoneRatings($values['xtra']);
      if ($r_num > 0) {
        $args = ['%num' => $r_num];
        $msg = t('WARNING: Updating will remove already done single ratings (number=%num).', $args);
        $form['message'] = [
            '#type' => 'markup',
            '#markup' => SlogXt::htmlMessage($msg, 'warning'),
        ];
      }

      $this->old_content = $values['content'];
      $this->addFieldXtContent($form, 'content', t('Text'), $values['content']);
      $form['content']['#required'] = TRUE;
      $this->addFieldActionSubmit($form, t('Update'));
    }
    else {
      $args = [
          '%nid' => $this->node->id(),
          '%itemid' => $xtitem_id,
      ];
      $msg = t('Item not found (nid=%nid, iid=%itemid)', $args);
      $form['message'] = [
          '#type' => 'markup',
          '#markup' => SlogXt::htmlMessage($msg, 'error'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $input = (string) $values['content'];
    if ($input === $this->old_content) {
      $form_state->setErrorByName('content', t('No changes have been done.'));
      return;
    }

    $input_array = $this->splitInput($input);
    if (empty($input_array)) {
      $form_state->setErrorByName('content', t('Text field is required.'));
    }
    elseif (count($input_array) > 1) {
      $args = ['%sep' => $this->getSeparator()];
      $form_state->setErrorByName('content', t('Multiple items with leading "%sep" is not allowed.', $args));
    }
    else {
      $this->input = $input_array[0];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $field_name = $this->getFieldName();
    $xtitems = $this->node->get($field_name);
    $xtitem = $xtitems->get($this->delta);
    $new_values = $xtitem->getValue();
    $new_values['content'] = $this->input;
    if (!empty($new_values['xtra'])) {
      $new_values['xtra'] = $this->unsetRatedState($new_values['xtra']);
    }

    $xtitems->set($this->delta, $new_values);
    $this->node->save();
    
    \Drupal::messenger()->addStatus(t('Item has been updated'));
  }

}
