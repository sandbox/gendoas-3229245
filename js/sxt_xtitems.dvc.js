/**
 * @file
 * sxt_xtitems/js/sxt_xtitems.dvc.js
 */

(function ($, Drupal, _) {

  var _sxt = Drupal.slogxt;

  var xtitemsExt = {
    _xtiItemsFormAttach: function ($content) {
      if ($content.find('.node-xtitems-edit-form, .node-xttaract-edit-form').length) {
        var $dropbuttons = $content.find('.dropbutton-widget')
            , $dbul = $dropbuttons.find('ul.xtsitems-ops');
//        $content.find('a.xtsitems-settings, a.xtsitems-add').once('xtc')
        $(once('xtc', 'a.xtsitems-settings, a.xtsitems-add', $content[0]))
            .on('click', $.proxy(this._xtiItemsRun, this));
        if ($dbul.length) {
//          $dropbuttons.find('.dropbutton-toggle').once('dropbutton-toggle')
          $(once('dropbutton-toggle', $dropbuttons.find('.dropbutton-toggle').toArray()))
              .on('click', _sxt.dropbuttonClickHandler);
//          $dbul.find('li>a').once('xtc')
          $(once('xtc', $dbul.find('li>a').toArray()))
              .on('click', $.proxy(this._xtiItemsRun, this));
        }
      }

//      $content.find('#xtitems_preset').once('xtc')
      $(once('xtc', '#xtitems_preset', $content[0]))
          .on('change', $.proxy(this._xtiItemsPreset, this));
      this._sxtFormPreviewAttach($content);
    },
    _xtiItemsPreset: function (e) {
      var $form = $(e.target).closest('form')
          , value = $(e.target).val()
          , labels = _.isString(value) ? value.split(';') : [];
      if (_.size(labels) === 5) {
        _.each(labels, function (label, idx) {
          $form.find('#xtitems_region_' + (4 - idx)).val(label);
        });
      }
    },
    _xtiItemsRun: function (e) {
      e.preventDefault();
      e.stopPropagation();
      var curPageIdx = this.getWizardCurPageIdx()
          , href01 = $(e.target).attr('href')
          , href02 = (href01.split('?')[0]).split('/xtajx/')[1] || false
          , newHref = href02 ? 'sxtxti/xtajx/' + href02 : false;

      if (curPageIdx > 0 && newHref) {
        var pageData = this._sxtWizardPageData(curPageIdx - 1);
        pageData.$tab && pageData.$tab.click();
        this._sxtWizardPagesDestroy(curPageIdx);
        this.overridePath = newHref;
        this.$btnSubmit.click();
      } else {
        href01 = href01;
      }
    }
  };

  $.extend(true, _sxt.DialogViewBase.prototype, xtitemsExt);

})(jQuery, Drupal, _);
